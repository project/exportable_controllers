<?php

namespace Drupal\exportable_controllers\Plugin\simple_sitemap\UrlGenerator;

use Drupal\Core\Url;
use Drupal\exportable_controllers\Entity\ExportableControllerEntity;
use Drupal\simple_sitemap\Plugin\simple_sitemap\UrlGenerator\EntityUrlGeneratorBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\simple_sitemap\Simplesitemap;
use Drupal\simple_sitemap\EntityHelper;
use Drupal\simple_sitemap\Logger;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

/**
 * Views URL generator plugin.
 *
 * @UrlGenerator(
 *   id = "exportable_controller",
 *   label = @Translation("Exportable controller URL generator"),
 *   description = @Translation("Generates URLs for exportable controllers."),
 * )
 */
class ExportableControllerUrlGenerator extends EntityUrlGeneratorBase {

  /**
   * The route provider.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * ViewsUrlGenerator constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\simple_sitemap\Simplesitemap $generator
   *   The simple_sitemap.generator service.
   * @param \Drupal\simple_sitemap\Logger $logger
   *   The simple_sitemap.logger service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\simple_sitemap\EntityHelper $entity_helper
   *   The simple_sitemap.entity_helper service.
   * @param \Drupal\Core\Routing\RouteProviderInterface $route_provider
   *   The route provider.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    Simplesitemap $generator,
    Logger $logger,
    LanguageManagerInterface $language_manager,
    EntityTypeManagerInterface $entity_type_manager,
    EntityHelper $entity_helper,
    RouteProviderInterface $route_provider
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $generator,
      $logger,
      $language_manager,
      $entity_type_manager,
      $entity_helper
    );
    $this->routeProvider = $route_provider;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('simple_sitemap.generator'),
      $container->get('simple_sitemap.logger'),
      $container->get('language_manager'),
      $container->get('entity_type.manager'),
      $container->get('simple_sitemap.entity_helper'),
      $container->get('router.route_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDataSets() {
    $data_sets = [];

    // Get data sets.
    $exportableControllers = $this->entityTypeManager->getStorage('exportable_controller_entity')->loadByProperties([
      'status' => TRUE,
    ]);
    /** @var \Drupal\exportable_controllers\Entity\ExportableControllerEntityInterface $exportableController */
    foreach ($exportableControllers as $exportableController) {
      if ($exportableController->isInSitemap()) {
        $data_sets[] = $exportableController->id();
      }
    }

    return $data_sets;
  }

  /**
   * {@inheritdoc}
   */
  protected function processDataSet($data_set) {
    /** @var \Drupal\exportable_controllers\Entity\ExportableControllerEntityInterface $exportableController */
    $exportableController = $this->entityTypeManager->getStorage('exportable_controller_entity')->load($data_set);

    if (!$exportableController) {
      return FALSE;
    }

    $exportableController = ExportableControllerEntity::getTranslated($exportableController);

    $url = Url::fromRoute(
      'entity.exportable_controller_entity.route.' . $exportableController->id(),
      [],
      ['language' => $this->languages[$this->defaultLanguageId]]
    );
    $url->setAbsolute();

    $alternateUrls = [
      $this->defaultLanguageId => $url->toString(TRUE)->getGeneratedUrl(),
    ];
    foreach ($this->languages as $language) {
      $routeName = 'entity.exportable_controller_entity.route.' . $exportableController->id() . '.' . $language->getId();
      try {
        /** @var \Drupal\Core\Url $urlTrans */
        $urlTrans = Url::fromRoute(
          $routeName,
          [],
          ['language' => $language]
        );
        $urlTrans->setAbsolute();
        $alternateUrls[$language->getId()] = $urlTrans->toString(TRUE)->getGeneratedUrl();
      }
      catch (RouteNotFoundException $e) {
        // Add default route.
        $routeName = 'entity.exportable_controller_entity.route.' . $exportableController->id();
        /** @var \Drupal\Core\Url $urlTrans */
        $urlTrans = Url::fromRoute(
          $routeName,
          [],
          ['language' => $language]
        );
        $urlTrans->setAbsolute();
        $alternateUrls[$language->getId()] = $urlTrans->toString(TRUE)->getGeneratedUrl();
      }
    }

    $data = [
      'url' => $url->toString(TRUE)->getGeneratedUrl(),
      'lastmod' => NULL,
      'priority' => '1.0',
      'changefreq' => 'daily',
      'images' => [],
      'langcode' => $this->defaultLanguageId,
      // Additional info useful in hooks.
      'meta' => [
        'path' => $exportableController->getPath(),
        'exportable_controller_info' => [
          'exportable_controller_id' => $data_set,
        ],
      ],
    ];

    if (count($alternateUrls) > 0) {
      $data['alternate_urls'] = $alternateUrls;
    }

    return $data;
  }

}
