<?php

namespace Drupal\exportable_controllers\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\exportable_controllers\Entity\ExportableControllerEntity;

/**
 * Returns responses for exportable_controllers routes.
 */
class ExportableControllersController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {
    // This a dummy controller that doesn't must output anything.
    return [];
  }

  /**
   * Provides the page title for this controller.
   *
   * @param \Drupal\exportable_controllers\Entity\ExportableControllerEntityInterface|null $entity
   *   Current entity.
   *
   * @return string
   *   The page title.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getTitle($entity = NULL) {
    $exportable = $this->entityTypeManager()->getStorage('exportable_controller_entity')->load($entity);
    if ($exportable) {
      $currentLanguage = $this->languageManager()->getCurrentLanguage();
      $exportableTrans = ExportableControllerEntity::getTranslated($exportable, $currentLanguage);
      if ($exportableTrans) {
        return $exportableTrans->label();
      }
    }

    return 'Page';
  }

}
