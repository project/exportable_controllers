<?php

namespace Drupal\exportable_controllers\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\PermissionHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Drupal\Core\Routing\RouteBuilderInterface;

/**
 * ExportableControllerEntityForm form controller.
 */
class ExportableControllerEntityForm extends EntityForm {

  /**
   * The permission handler.
   *
   * @var \Drupal\user\PermissionHandlerInterface
   */
  protected $permissionHandler;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Router builder.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routeBuilder;

  /**
   * Constructs a object.
   *
   * @param \Drupal\user\PermissionHandlerInterface $permission_handler
   *   The permission handler.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Routing\RouteBuilderInterface $routeBuilder
   *   Router builder.
   */
  public function __construct(
    PermissionHandlerInterface $permission_handler,
    ModuleHandlerInterface $module_handler,
    RouteBuilderInterface $routeBuilder
  ) {
    $this->permissionHandler = $permission_handler;
    $this->moduleHandler = $module_handler;
    $this->routeBuilder = $routeBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.permissions'),
      $container->get('module_handler'),
      $container->get('router.builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\exportable_controllers\Entity\ExportableControllerEntityInterface $exportable_controller_entity */
    $exportable_controller_entity = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#maxlength' => 255,
      '#default_value' => $exportable_controller_entity->label(),
      '#description' => $this->t("Title for the Exportable controller."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $exportable_controller_entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\exportable_controllers\Entity\ExportableControllerEntity::load',
      ],
      '#disabled' => !$exportable_controller_entity->isNew(),
    ];

    $form['_controller'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Callable controller'),
      '#description' => $this->t('A class name with namespace and method to call, you can leave empty to use default controller: \Drupal\exportable_controllers\Controller\ExportableControllersController::build'),
      '#default_value' => $exportable_controller_entity->getCallableController(),
    ];

    $form['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path'),
      '#description' => $this->t('Router path. ex. /mypage'),
      '#required' => TRUE,
      '#default_value' => $exportable_controller_entity->getPath(),
    ];

    $form['not_sitemap'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Not show in sitemap'),
      '#description' => $this->t('Useful if we have a sitemap generator module that uses this value.'),
      '#default_value' => !$exportable_controller_entity->isInSitemap(),
    ];

    $form['requirements'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Requirements'),
    ];

    $form['requirements']['_access'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Open to all users'),
      '#default_value' => $exportable_controller_entity->canAccess(),
    ];

    // Get list of permissions.
    $perms = [];
    $permissions = $this->permissionHandler->getPermissions();
    foreach ($permissions as $perm => $perm_item) {
      $provider = $perm_item['provider'];
      $display_name = $this->moduleHandler->getName($provider);
      $perms[$display_name][$perm] = strip_tags($perm_item['title']);
    }
    $form['requirements']['_permission'] = [
      '#type' => 'select',
      '#options' => $perms,
      '#empty_option' => $this->t('- Select -'),
      '#title' => $this->t('Permission'),
      '#default_value' => $exportable_controller_entity->getPermission(),
      '#description' => $this->t('Only users with the selected permission flag will be able to access this path.'),
    ];

    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    $options = [];
    foreach ($roles as $id => $role) {
      $options[$id] = $role->label();
    }
    $form['requirements']['_role'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Required user roles'),
      '#empty_option' => $this->t('- Select -'),
      '#options' => $options,
      '#default_value' => explode(',', $exportable_controller_entity->getRole()),
    ];

    $form['requirements']['_custom_access'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Controller access'),
      '#description' => $this->t('A class name with namespace and method to call, you can leave empty to not use it.'),
      '#default_value' => $exportable_controller_entity->getCustomAccess(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $exportable_controller_entity->status(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $aux = [
      '_access' => $values['_access'] ? 'TRUE' : 'FALSE',
      '_permission' => $values['_permission'],
      '_role' => implode(',', array_keys($values['_role'])),
      '_custom_access' => $values['_custom_access'],
    ];
    if (empty($aux['_permission'])) {
      unset($aux['_permission']);
    }
    if (empty($aux['_custom_access'])) {
      unset($aux['_custom_access']);
    }
    $route = new Route(
      $values['path'],
      [
        '_controller' => $values['_controller'],
        '_title' => $values['label'],
      ],
      $aux
    );
    $route->compile();
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\exportable_controllers\Entity\ExportableControllerEntityInterface $exportable_controller_entity */
    $exportable_controller_entity = $this->entity;

    $exportable_controller_entity->setPath($form_state->getValue('path'));

    $exportable_controller_entity->setCallableController($form_state->getValue('_controller'));

    $access = $form_state->getValue('_access');
    $exportable_controller_entity->setAccess(boolval($access));

    $exportable_controller_entity->setPermission($form_state->getValue('_permission'));

    $roles = $form_state->getValue('_role');
    $fRole = [];
    foreach ($roles as $rkey => $role) {
      if ($role !== 0) {
        $fRole[] = $rkey;
      }
    }
    $exportable_controller_entity->setRole(implode(',', $fRole));

    $exportable_controller_entity->setCustomAccess($form_state->getValue('_custom_access'));

    $exportable_controller_entity->setInSiteMap(!$form_state->getValue('not_sitemap'));

    $status = $exportable_controller_entity->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Exportable controller.', [
          '%label' => $exportable_controller_entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Exportable controller.', [
          '%label' => $exportable_controller_entity->label(),
        ]));
    }
    // Rebuild the menu router based on all rebuilt data.
    // Important: This rebuild must happen last, so the menu router is
    // guaranteed to be based on up to date information.
    $this->routeBuilder->rebuild();

    // Redirect user.
    $form_state->setRedirectUrl($exportable_controller_entity->toUrl('collection'));
  }

}
