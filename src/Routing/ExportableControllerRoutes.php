<?php

namespace Drupal\exportable_controllers\Routing;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\exportable_controllers\Entity\ExportableControllerEntity;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Defines a route subscriber to register a url configurable paths.
 */
class ExportableControllerRoutes implements ContainerInjectionInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a new ExportableControllerRoutes object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   Language manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    LanguageManagerInterface $languageManager
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('language_manager')
    );
  }

  /**
   * Returns an array of route objects.
   *
   * @return \Symfony\Component\Routing\Route[]
   *   An array of route objects.
   */
  public function routes() {
    $routes = [];

    $expRoutes = $this->entityTypeManager->getStorage('exportable_controller_entity')->loadMultiple();
    /** @var \Drupal\exportable_controllers\Entity\ExportableControllerEntityInterface $exportableController */
    foreach ($expRoutes as $exportableController) {
      /** @var \Drupal\exportable_controllers\Entity\ExportableControllerEntityInterface $exportableControllerDefault */
      $exportableControllerDefault = ExportableControllerEntity::getTranslated($exportableController);
      if ($exportableController->status()) {
        $aux = [
          '_access' => $exportableController->canAccess() ? 'TRUE' : NULL,
          '_permission' => $exportableController->getPermission(),
          '_custom_access' => $exportableController->getCustomAccess(),
        ];
        if (empty($aux['_access'])) {
          unset($aux['_access']);
        }
        if (!empty($exportableController->getRole())) {
          $aux['_role'] = $exportableController->getRole();
        }
        if (empty($aux['_custom_access'])) {
          unset($aux['_custom_access']);
        }
        if (empty($aux['_permission'])) {
          unset($aux['_permission']);
        }

        $routes['entity.exportable_controller_entity.route.' . $exportableController->id()] = new Route(
          $exportableControllerDefault->getPath(),
          [
            '_controller' => $exportableController->getCallableController(),
            '_title_callback' => '\Drupal\exportable_controllers\Controller\ExportableControllersController::getTitle',
            'entity' => $exportableController->id(),
          ],
          $aux
        );

        // Prepare translated versions.
        $languages = $this->languageManager->getLanguages();
        unset($languages[$this->languageManager->getDefaultLanguage()->getId()]);
        foreach ($languages as $language) {
          /** @var \Drupal\exportable_controllers\Entity\ExportableControllerEntityInterface $exportableControllerDefault */
          $exportableControllerTrans = ExportableControllerEntity::getTranslated($exportableControllerDefault, $language);
          if (!$exportableControllerTrans->isNew()) {
            if ($exportableControllerDefault->label() != $exportableControllerTrans->label()) {
              $routes['entity.exportable_controller_entity.route.' . $exportableController->id() . '.' . $language->getId()] = new Route(
                $exportableControllerTrans->getPath(),
                [
                  '_controller' => $exportableController->getCallableController(),
                  '_title_callback' => '\Drupal\exportable_controllers\Controller\ExportableControllersController::getTitle',
                  'entity' => $exportableController->id(),
                ],
                $aux
              );
            }
          }
        }
      }
    }

    return $routes;
  }

}
