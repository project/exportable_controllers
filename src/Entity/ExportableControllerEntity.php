<?php

namespace Drupal\exportable_controllers\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Link;

/**
 * Defines the Exportable controller entity.
 *
 * @ConfigEntityType(
 *   id = "exportable_controller_entity",
 *   label = @Translation("Exportable controller"),
 *   label_collection = @Translation("Exportable controller library"),
 *   label_singular = @Translation("Exportable controller"),
 *   label_plural = @Translation("Exportable controllers"),
 *   label_count = @PluralTranslation(
 *     singular = "@count exportable controller",
 *     plural = "@count Exportable controllers",
 *   ),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\exportable_controllers\ExportableControllerEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\exportable_controllers\Form\ExportableControllerEntityForm",
 *       "edit" = "Drupal\exportable_controllers\Form\ExportableControllerEntityForm",
 *       "delete" = "Drupal\exportable_controllers\Form\ExportableControllerEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\exportable_controllers\ExportableControllerEntityHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "exportable_controller_entity",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/exportable_controller_entity/add",
 *     "edit-form" = "/admin/structure/exportable_controller_entity/{exportable_controller_entity}/edit",
 *     "delete-form" = "/admin/structure/exportable_controller_entity/{exportable_controller_entity}/delete",
 *     "collection" = "/admin/structure/exportable_controller_entity",
 *     "canonical" = "/admin/exportable_controller/{exportable_controller_entity}"
 *   },
 *   config_export = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "status",
 *     "path",
 *     "controller",
 *     "access",
 *     "permission",
 *     "role",
 *     "custom_access",
 *     "not_sitemap",
 *   }
 * )
 */
class ExportableControllerEntity extends ConfigEntityBase implements ExportableControllerEntityInterface {

  /**
   * The Exportable controller ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Exportable controller label.
   *
   * @var string
   */
  protected $label;

  /**
   * Controller path.
   *
   * @var string
   */
  protected $path;

  /**
   * Controller handler.
   *
   * @var string
   */
  protected $controller;

  /**
   * Open access.
   *
   * @var bool
   */
  protected $access;

  /**
   * Permission ID.
   *
   * @var string
   */
  protected $permission = 'access content';

  /**
   * Role list.
   *
   * @var string
   */
  protected $role = '';

  /**
   * Custom access handler.
   *
   * @var string
   */
  protected $custom_access;

  /**
   * TRUE to not show in sitemap.
   *
   * @var bool
   */
  protected $not_sitemap = FALSE;

  /**
   * {@inheritdoc}
   */
  public function toLink($text = NULL, $rel = 'canonical', array $options = []) {
    if (!isset($text)) {
      $text = $this->label();
    }
    if ($rel == 'canonical') {
      $rel = 'entity.exportable_controller_entity.route.' . $this->id;
    }

    return Link::createFromRoute($text, $rel, [], $options);
  }

  /**
   * Get translated config entity.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $configEntity
   *   The config entity to translate.
   * @param \Drupal\Core\Language\LanguageInterface|null $language
   *   The translation language to get.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Translated config entity.
   *
   * @see https://drupal.stackexchange.com/a/270095
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function getTranslated(ConfigEntityInterface $configEntity, LanguageInterface $language = NULL) {
    if (!$language) {
      $language = \Drupal::languageManager()->getDefaultLanguage();
    }
    $currentLanguage = \Drupal::languageManager()->getConfigOverrideLanguage();
    \Drupal::languageManager()->setConfigOverrideLanguage($language);
    $translatedConfigEntity = \Drupal::entityTypeManager()
      ->getStorage($configEntity->getEntityTypeId())
      ->load($configEntity->id());
    \Drupal::languageManager()->setConfigOverrideLanguage($currentLanguage);
    $translatedConfigEntity->set('langcode', $language->getId());
    return $translatedConfigEntity;
  }

  /**
   * {@inheritdoc}
   */
  public function getPath() {
    return $this->path;
  }

  /**
   * {@inheritdoc}
   */
  public function setPath($value) {
    $this->path = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCallableController() {
    if (empty($this->controller)) {
      return '\Drupal\exportable_controllers\Controller\ExportableControllersController::build';
    }
    return $this->controller;
  }

  /**
   * {@inheritdoc}
   */
  public function setCallableController($value) {
    $this->controller = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function canAccess() {
    return $this->access;
  }

  /**
   * {@inheritdoc}
   */
  public function setAccess(bool $access) {
    $this->access = $access;
  }

  /**
   * {@inheritdoc}
   */
  public function getPermission() {
    return $this->permission;
  }

  /**
   * {@inheritdoc}
   */
  public function setPermission(string $permission) {
    $this->permission = $permission;
  }

  /**
   * {@inheritdoc}
   */
  public function getRole() {
    return $this->role;
  }

  /**
   * {@inheritdoc}
   */
  public function setRole(string $role) {
    $this->role = $role;
  }

  /**
   * {@inheritdoc}
   */
  public function getCustomAccess() {
    return $this->custom_access;
  }

  /**
   * {@inheritdoc}
   */
  public function setCustomAccess(string $custom_access) {
    $this->custom_access = $custom_access;
  }

  /**
   * {@inheritdoc}
   */
  public function isInSitemap() {
    return !$this->not_sitemap;
  }

  /**
   * {@inheritdoc}
   */
  public function setInSiteMap($addIt = TRUE) {
    $this->not_sitemap = !$addIt;
  }

}
