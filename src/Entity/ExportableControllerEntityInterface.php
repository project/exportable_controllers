<?php

namespace Drupal\exportable_controllers\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Exportable controller entities.
 */
interface ExportableControllerEntityInterface extends ConfigEntityInterface {

  /**
   * Controller path.
   *
   * @return string
   *   The path.
   */
  public function getPath();

  /**
   * Set controller path.
   *
   * @param string $value
   *   The path.
   *
   * @return \Drupal\exportable_controllers\Entity\ExportableControllerEntityInterface
   *   This entity.
   */
  public function setPath($value);

  /**
   * Controller handler.
   *
   * @return string
   *   The callable.
   */
  public function getCallableController();

  /**
   * Set controller handler.
   *
   * @param string $value
   *   New value.
   *
   * @return \Drupal\exportable_controllers\Entity\ExportableControllerEntityInterface
   *   This entity.
   */
  public function setCallableController($value);

  /**
   * Access.
   *
   * @return bool
   *   Access.
   */
  public function canAccess();

  /**
   * Set access.
   *
   * @param bool $access
   *   New value.
   */
  public function setAccess(bool $access);

  /**
   * Get permission.
   *
   * @return string
   *   Permission.
   */
  public function getPermission();

  /**
   * Set permissions.
   *
   * @param string $permission
   *   New value.
   */
  public function setPermission(string $permission);

  /**
   * Get roles.
   *
   * @return string
   *   Roles.
   */
  public function getRole();

  /**
   * Set roles.
   *
   * @param string $role
   *   New value.
   */
  public function setRole(string $role);

  /**
   * Get custom access.
   *
   * @return string
   *   Custom access callable.
   */
  public function getCustomAccess();

  /**
   * Set custom access.
   *
   * @param string $custom_access
   *   New value.
   */
  public function setCustomAccess(string $custom_access);

  /**
   * Is in sitemap?
   *
   * @return bool
   *   Is in sitemap?
   */
  public function isInSitemap();

  /**
   * Must be in sitemap?
   *
   * @param bool $addIt
   *   Must be in sitemap?
   */
  public function setInSiteMap($addIt = TRUE);

}
