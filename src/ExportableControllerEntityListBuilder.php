<?php

namespace Drupal\exportable_controllers;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Exportable controller entities.
 */
class ExportableControllerEntityListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   *
   * Builds the entity listing as renderable array for table.html.twig.
   *
   * @todo Add a link to add a new item to the #empty text.
   */
  public function render() {
    $build = parent::render();
    $build['table']['#cache']['contexts'][] = 'languages:language_content';

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Exportable controller');
    $header['id'] = $this->t('Machine name');
    $header['path'] = $this->t('Path');
    $header['status'] = $this->t('Active');
    $header['sitemap'] = $this->t('In sitemap');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\exportable_controllers\Entity\ExportableControllerEntityInterface $entity */
    $label = [
      '#markup' => $entity->label(),
    ];
    if ($entity->status()) {
      $path = $entity->getPath();
      if (substr($entity->getPath(), 0, 1) != '/') {
        $path = '/' . $path;
      }
      $label = $entity->toLink()->toRenderable();
    }
    $row['label'] = \Drupal::service('renderer')->render($label);
    $row['id'] = $entity->id();
    $row['Path'] = $entity->getPath();
    $row['status'] = $entity->status() ? $this->t('Enabled') : $this->t('Disabled');
    $row['sitemap'] = $entity->isInSitemap() ? $this->t('Yes') : $this->t('No');
    return $row + parent::buildRow($entity);
  }

}
