<?php

namespace Drupal\exportable_controllers_metatag;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\Language;
use Drupal\metatag\MetatagManager;
use Drupal\exportable_controllers\Entity\ExportableControllerEntity;
use Drupal\exportable_controllers\Entity\ExportableControllerEntityInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\metatag\MetatagTagPluginManager;

/**
 * Implement common functionality.
 */
class MetatagsManager {

  /**
   * An array of base language data.
   *
   * @var array
   */
  protected $baseData = [];

  /**
   * The metatag.manager service.
   *
   * @var \Drupal\metatag\MetatagManager
   */
  protected $metatagManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Metatag tag plugin manager.
   *
   * @var \Drupal\metatag\MetatagTagPluginManager
   */
  protected $metatagTagPluginManager;

  /**
   * Constructs a MetatagsManager object.
   *
   * @param \Drupal\metatag\MetatagManager $metatag_manager
   *   The metatag.manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   Language manager.
   * @param \Drupal\metatag\MetatagTagPluginManager $metatagTagPluginManager
   *   Metatag tag plugin manager.
   */
  public function __construct(
    MetatagManager $metatag_manager,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $configFactory,
    LanguageManagerInterface $languageManager,
    MetatagTagPluginManager $metatagTagPluginManager
  ) {
    $this->metatagManager = $metatag_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $configFactory;
    $this->languageManager = $languageManager;
    $this->metatagTagPluginManager = $metatagTagPluginManager;
  }

  /**
   * Gets the translated values while storing a copy of the original values.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   * @param \Drupal\Core\Language\Language $language
   *   Language.
   */
  public function prepareTranslatedValues(FormStateInterface $form_state, Language $language) {
    /** @var \Drupal\Core\Routing\RouteMatch $routeMatch */
    $routeMatch = $form_state->getBuildInfo()['args'][0];
    $exportableController = $routeMatch->getParameter('exportable_controller_entity');

    $entity = $exportableController;

    $config_name = 'exportable_controllers.exportable_controller_entity.' . $entity->id();
    $config_path = 'metatags';

    $configuration = $this->configFactory->get($config_name);
    $this->baseData = $configuration->getOriginal($config_path, FALSE);
    if (!$this->baseData) {
      $this->baseData = [];
    }

    // Set the translation target language on the configuration factory.
    $original_language = $this->languageManager->getConfigOverrideLanguage();
    $this->languageManager->setConfigOverrideLanguage($language);

    // Read in translated values.
    $configuration = $this->configFactory->get($config_name);
    $translated_values = $configuration->get($config_path);
    if (!$translated_values) {
      $translated_values = [];
    }

    // Set the configuration language back.
    $this->languageManager->setConfigOverrideLanguage($original_language);

    return $translated_values;
  }

  /**
   * Add the translation form element for meta tags available in the source.
   */
  public function translatedForm(array $element, array $translated_values) {
    $translated_values = $this->clearMetatagDisallowedValues($translated_values);
    // Only offer form elements for tags present in the source language.
    $source_values = $this->removeEmptyTags($this->baseData);

    // Add the outer fieldset.
    $element += [
      '#type' => 'details',
    ];

    foreach ($source_values as $tag_id => $value) {
      $tag = $this->metatagTagPluginManager->createInstance($tag_id);
      $tag->setValue($translated_values[$tag_id]);

      $form_element = $tag->form($element);
      $element[$tag_id] = [
        '#theme' => 'config_translation_manage_form_element',
        'source' => [
          '#type' => 'item',
          '#title' => $form_element['#title'],
        ],
        'translation' => $form_element,
      ];
      if (is_string($value)) {
        $element[$tag_id]['source']['#markup'] = $value;
      }
      elseif ($form_element['#type'] == 'checkboxes') {
        $options = $form_element["#options"];
        $lines = [];
        /**
         * @var string $key
         * @var \Drupal\Core\StringTranslation\TranslatableMarkup $option
         */
        foreach ($options as $key => $option) {
          if ($value[$key] === $key) {
            $lines[] = $option->render();
          }
        }
        $element[$tag_id]['source']['#markup'] = '<ul>';
        /** @var \Drupal\Core\StringTranslation\TranslatableMarkup $line */
        foreach ($lines as $line) {
          $element[$tag_id]['source']['#markup'] .= '<li>' . $line . '</li>';
        }
        $element[$tag_id]['source']['#markup'] .= '</ul>';
      }
    }

    return $element;
  }

  /**
   * Gets the meta tags of a specific exportable_controller, if set.
   *
   * @param mixed $exportableController
   *   The exportable_controller id, or exportable_controller config entity.
   *
   * @return array|null
   *   The meta tags if set, null otherwise.
   */
  public function getTags($exportableController) {
    if (empty($exportableController)) {
      return;
    }
    if (is_string($exportableController)) {
      $entity = ExportableControllerEntity::load($exportableController);
    }
    else {
      $entity = $exportableController;
    }
    if (!$entity instanceof ExportableControllerEntityInterface) {
      return;
    }

    // Retrieve the metatag settings from the extender.
    $metatags = [];

    if (!empty($entity->metatags)) {
      $metatags = $entity->metatags;
    }

    return $metatags;
  }

  /**
   * Clears the metatag form state values from illegal elements.
   *
   * @param array $metatags
   *   Array of values to submit.
   *
   * @return array
   *   Filtered metatag array.
   */
  public function clearMetatagDisallowedValues(array $metatags) {
    // Get all legal tags.
    $tags = $this->metatagManager->sortedTags();

    // Return only common elements.
    $metatags = array_intersect_key($metatags, $tags);

    return $metatags;
  }

  /**
   * Removes tags that are empty.
   */
  public function removeEmptyTags($metatags) {
    $metatags = array_filter($metatags, function ($value) {
      if (is_array($value)) {
        return count(array_filter($value)) > 0;
      }
      else {
        return $value !== '';
      }
    });
    return $metatags;
  }

}
