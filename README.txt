CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module allow define new routes by UI and export it into configurations
files.

A use case it's to use block layout in custom paths. Define a new route and
then set blocks in it.


 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/exportable_controllers

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/exportable_controllers

REQUIREMENTS
------------

This module not requires other modules.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

Manage exportable routes in /admin/structure/exportable_controller_entity.

MAINTAINERS
-----------

Current maintainers:
 * Pedro Pelaez (psf_) - https://www.drupal.org/u/psf_
